import {Injectable} from '@angular/core';
import {ParcelService} from '../domain/ports/parcel-service';
import {catchError, Observable} from 'rxjs';
import {Parcel} from '../domain/models/parcel';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ParcelOperationError} from '../domain/error/ParcelOpertionError';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HappyParcelService implements ParcelService {
  parcelUrl = environment.apiUrl + '/parcel';

  constructor(private http: HttpClient) {
  }

  fetchParcels(
    byCountry?: string,
    byDescription?: string
  ): Observable<Parcel[]> {
    let queryParams = new HttpParams();
    if (byCountry) {
      queryParams = queryParams.append('byCountry', byCountry);
    }
    if (byDescription) {
      queryParams = queryParams.append('byDescription', byDescription);
    }
    let getParcelsObservable;
    if (queryParams.keys().length > 0) {
      getParcelsObservable = this.http.get<Parcel[]>(this.parcelUrl + '?' + queryParams.toString());
    } else {
      getParcelsObservable = this.http.get<Parcel[]>(this.parcelUrl);
    }
    return getParcelsObservable.pipe(
      catchError(this.handleHttpError())
    );
  }

  fetchParcelsByCountry(byCountry: string): Observable<Parcel[]> {
    return this.fetchParcels(byCountry, undefined);
  }

  fetchParcelsByDescription(byDescription: string): Observable<Parcel[]> {
    return this.fetchParcels(undefined, byDescription);
  }

  fetchParcelCountries(): Observable<string[]> {
    const url = this.parcelUrl + '/countries';
    return this.http.get<string[]>(url).pipe(
      catchError(this.handleHttpError())
    );
  }

  addParcel(parcel: Parcel): Observable<Parcel> {
    return this.http.post<Parcel>(this.parcelUrl, parcel).pipe(
      catchError(this.handleHttpError())
    );
  }

  /**
   * Handle Http operation that failed.
   * Throw an HeroOperation
   */
  private handleHttpError() {
    return (error: any): Observable<any> => {
      console.log(error)
      throw new ParcelOperationError(error.message);
    };
  }
}
