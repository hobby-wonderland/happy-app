import {Parcel} from '../domain/models/parcel';
import {ParcelSKU} from '../domain/models/parcel-sku';

/**
 * Mock parcels data for POC
 */
export const MOCK_PARCELS: Parcel[] = [
  {
    sku: new ParcelSKU('1'),
    description: 'First parcel with toys',
    streetAddress: 'Tehnika 1',
    town: 'Tallinn',
    country: 'Estonia',
    deliveryAt: new Date(2022, 3, 11),
  },
  {
    sku: new ParcelSKU('2'),
    description: 'Second parcel',
    streetAddress: 'Jaama 1',
    town: 'Tartu',
    country: 'Estonia',
    deliveryAt: new Date(2022, 5, 12),
  },
  {
    sku: new ParcelSKU('3'),
    description: 'Third parcel',
    streetAddress: 'Zeltiņu 10',
    town: 'Riga',
    country: 'Latvia',
    deliveryAt: new Date(2022, 6, 11),
  },
  {
    sku: new ParcelSKU('4'),
    description: 'Fourth parcel',
    streetAddress: 'Brucenes 25',
    town: 'Riga',
    country: 'Latvia',
    deliveryAt: new Date(2022, 6, 20),
  },
  {
    sku: new ParcelSKU('5'),
    description: 'Fifth parcel with toys',
    streetAddress: 'Sinipiianpolku 3',
    town: 'Helsinki',
    country: 'Finland',
    deliveryAt: new Date(2022, 3, 4),
  },
  {
    sku: new ParcelSKU('6'),
    description: 'Sixth parcel',
    streetAddress: 'Kunnantie 2A',
    town: 'Helsinki',
    country: 'Finland',
    deliveryAt: new Date(2022, 3, 12),
  },
  {
    sku: new ParcelSKU('7'),
    description: 'Seventh parcel',
    streetAddress: 'Katarina Bangata 65',
    town: 'Stockholm',
    country: 'Sweden',
    deliveryAt: new Date(2022, 3, 11),
  },
  {
    sku: new ParcelSKU('8'),
    description: 'Eight parcel',
    streetAddress: 'Frejagatan 7A',
    town: 'Kalmar',
    country: 'Sweden',
    deliveryAt: new Date(2022, 3, 4),
  },
  {
    sku: new ParcelSKU('9'),
    description: 'Ninth parcel with toys',
    streetAddress: 'Tehnika 1',
    town: 'Narva',
    country: 'Estonia',
    deliveryAt: new Date(2022, 3, 5),
  },
];
