import {TestBed} from '@angular/core/testing';

import {HappyParcelService} from './happy-parcel.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Parcel} from '../domain/models/parcel';
import {of, throwError} from 'rxjs';

describe('ParcelService', () => {
  let service: HappyParcelService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  const TEST_API = 'test-api';


  beforeEach(() => {

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
    TestBed.configureTestingModule({
      providers: [{provide: HttpClient, useValue: httpClientSpy}]
    });
    service = TestBed.inject(HappyParcelService);
    service.parcelUrl = TEST_API;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected parcels', (done: DoneFn) => {
    const expectedParcels =
      [{
        sku: '1',
        description: 'A',
        country: 'B',
        town: 'C',
        deliveryAt: new Date(),
        streetAddress: 'D',
        registeredAt: new Date()
      }];

    httpClientSpy.get.and.returnValue(of(expectedParcels));

    service.fetchParcels().subscribe({
      next: parcels => {
        expect(parcels).toEqual(expectedParcels);
        done();
      },
      error: done.fail
    });
    expect(httpClientSpy.get).toHaveBeenCalledOnceWith(TEST_API);
  });

  it('should return an error getting parcels when the server returns a 404', (done: DoneFn) => {

    const errorResponse = {
      body: {error: 'test 404 error'},
      status: 404, statusText: 'Not Found'
    };

    httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

    service.fetchParcels().subscribe({
      next: () => done.fail('expected an error, not parcels'),
      error: error => {
        expect(error.name).toEqual('ParcelOperationError');
        expect(error.message).toContain('test 404 error');
        done();
      }
    });
  });

  it('should return searched parcels by country', (done: DoneFn) => {
    const parcelOne = {
      sku: '1',
      country: 'A',
      town: 'B',
      description: 'C',
      streetAddress: 'D',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const parcelTwo = {
      sku: '2',
      country: 'A',
      town: 'BB',
      description: 'CC',
      streetAddress: 'DD',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const expectedParcels: Parcel[] =
      [parcelOne, parcelTwo];

    httpClientSpy.get.and.returnValue(of(expectedParcels));

    service.fetchParcelsByCountry('A').subscribe({
      next: parcels => {
        expect(parcels).toEqual(expectedParcels);
        done();
      },
      error: done.fail
    });
    const params = new HttpParams().append('byCountry', 'A');
    expect(httpClientSpy.get).toHaveBeenCalledOnceWith(
      TEST_API + '?' + params.toString()
    );
  });

  it('should return an error when searching parcels by country when the server returns a 404', (done: DoneFn) => {

    const errorResponse = {
      body: {error: 'test 404 error'},
      status: 404, statusText: 'Not Found'
    };

    httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

    service.fetchParcelsByCountry('A').subscribe({
      next: () => done.fail('expected an error, not parcels'),
      error: error => {
        expect(error.name).toEqual('ParcelOperationError');
        expect(error.message).toContain('test 404 error');
        done();
      }
    });
  });

  it('should return searched parcels by description', (done: DoneFn) => {
    const parcelOne = {
      sku: '1',
      country: 'A',
      town: 'B',
      description: 'C',
      streetAddress: 'D',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const parcelTwo = {
      sku: '2',
      country: 'A',
      town: 'BB',
      description: 'CC',
      streetAddress: 'DD',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const expectedParcels: Parcel[] =
      [parcelOne, parcelTwo];

    httpClientSpy.get.and.returnValue(of(expectedParcels));

    service.fetchParcelsByDescription('C').subscribe({
      next: parcels => {
        expect(parcels).toEqual(expectedParcels);
        done();
      },
      error: done.fail
    });
    const params = new HttpParams().append('byDescription', 'C');
    expect(httpClientSpy.get).toHaveBeenCalledOnceWith(
      TEST_API + '?' + params.toString()
    );
  });

  it('should return an error when searching parcels by description when the server returns a 404', (done: DoneFn) => {

    const errorResponse = {
      body: {error: 'test 404 error'},
      status: 404, statusText: 'Not Found'
    };

    httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

    service.fetchParcelsByCountry('A').subscribe({
      next: () => done.fail('expected an error, not parcels'),
      error: error => {
        expect(error.name).toEqual('ParcelOperationError');
        expect(error.message).toContain('test 404 error');
        done();
      }
    });
  });

  it('should return searching parcels by country and description', (done: DoneFn) => {
    const parcelOne = {
      sku: '1',
      country: 'A',
      town: 'B',
      description: 'C',
      streetAddress: 'D',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const parcelTwo = {
      sku: '2',
      country: 'A',
      town: 'BB',
      description: 'CC',
      streetAddress: 'DD',
      deliveryAt: new Date(),
      registeredAt: new Date(),
    };
    const expectedParcels: Parcel[] =
      [parcelOne, parcelTwo];

    httpClientSpy.get.and.returnValue(of(expectedParcels));

    service.fetchParcels('A', 'C').subscribe({
      next: parcels => {
        expect(parcels).toEqual(expectedParcels);
        done();
      },
      error: done.fail
    });
    const params = new HttpParams().append('byCountry', 'A').append('byDescription', 'C');
    expect(httpClientSpy.get).toHaveBeenCalledOnceWith(
      TEST_API + '?' + params.toString()
    );
  });

  it('should return an error when searching parcels by country and description when the server returns a 404', (done: DoneFn) => {

    const errorResponse = {
      body: {error: 'test 404 error'},
      status: 404, statusText: 'Not Found'
    };

    httpClientSpy.get.and.returnValue(throwError(() => errorResponse));

    service.fetchParcels('A', 'C').subscribe({
      next: () => done.fail('expected an error, not parcels'),
      error: error => {
        expect(error.name).toEqual('ParcelOperationError');
        expect(error.message).toContain('test 404 error');
        done();
      }
    });
  });
});
