import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {ParcelListComponent} from './parcel-list/parcel-list.component';
import {ParcelAddComponent} from './parcel-add/parcel-add.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HappyParcelService} from './adapters/happy-parcel.service';

@NgModule({
  declarations: [
    AppComponent,
    ParcelListComponent,
    ParcelAddComponent,
    WelcomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: WelcomeComponent},
      {path: 'parcels', component: ParcelListComponent},
      {path: 'parcels/new', component: ParcelAddComponent},
    ]),
    ReactiveFormsModule,
  ],
  providers: [
    {provide: 'ParcelService', useClass: HappyParcelService},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
