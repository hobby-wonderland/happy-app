import {Parcel} from '../models/parcel';
import {Observable} from 'rxjs';

/**
 * Parcel Service interface
 */
export interface ParcelService {
  /**
   * Fetch parcels ordered by Country or byDescription
   * @param byCountry Country string value
   * @param byDescription Description string value
   * @returns {Observable<Parcel[]>} parcels where first results are from Estonia and the by delivery date ascending
   */
  fetchParcels(
    byCountry?: string,
    byDescription?: string
  ): Observable<Parcel[]>;

  /**
   * Fetch parcels by Country
   * @param byCountry
   * @returns {Observable<Parcel[]>} parcels where first results are from Estonia and the by delivery date ascending
   */
  fetchParcelsByCountry(byCountry: string): Observable<Parcel[]>;

  /**
   * Fetch parcels by their description
   * @param byDescription
   * @returns {Observable<Parcel[]>} parcels where first results are from Estonia and the by delivery date ascending
   */
  fetchParcelsByDescription(byDescription: string): Observable<Parcel[]>;

  /**
   * Fetch list of parcel countries that are being delivered to
   * @returns {Observable<string[]>} countries as an array of strings
   */
  fetchParcelCountries(): Observable<string[]>;

  /**
   * Add a new Parcel to the service
   * @param parcel new parcel
   * @returns parcel
   */
  addParcel(parcel: Parcel): Observable<Parcel>;
}
