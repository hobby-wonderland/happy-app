export interface Parcel {
  id?: string;
  sku: string;
  description: string;
  streetAddress: string;
  town: string;
  country: string;
  deliveryAt: Date;
  registeredAt?: Date;
  createdAt?: Date;
  updatedAt?: Date;
}
