export class ParcelOperationError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'ParcelOperationError';
  }
}
