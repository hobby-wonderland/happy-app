import {Component, Inject} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Parcel} from '../domain/models/parcel';
import {HappyParcelService} from '../adapters/happy-parcel.service';
import {ParcelService} from '../domain/ports/parcel-service';
import {ParcelOperationError} from '../domain/error/ParcelOpertionError';

@Component({
  selector: 'app-parcel-add',
  templateUrl: './parcel-add.component.html',
  styleUrls: ['./parcel-add.component.sass'],
  providers: [HappyParcelService]
})
export class ParcelAddComponent {
  form = this.formBuilder.group({
    sku: ['', Validators.required],
    description: ['', [Validators.required, Validators.maxLength(120)]],
    streetAddress: ['', [Validators.required]],
    town: ['', [Validators.required]],
    country: ['', Validators.required],
    deliveryDate: ['', Validators.required]
  });
  submitted = false;
  success = false;
  savedParcelSKU?: string = undefined;

  constructor(
    private formBuilder: FormBuilder,
    @Inject('ParcelService') private parcelService: ParcelService
  ) {
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.parcelService.addParcel(this.fromDataToParcel(this.form)).subscribe({
      next: (parcel) => {
        this.success = true;
        this.savedParcelSKU = parcel.sku;
        this.onReset();
      },
      error: (err: ParcelOperationError) => {
        console.log(err.message)
        this.form.controls.sku.setErrors({'incorrect': true})
      }
    });
  }

  onReset(): void {
    this.submitted = false;
    this.form.reset();
  }

  private fromDataToParcel(form: FormGroup): Parcel {
    return {
      country: form.value['country'],
      deliveryAt: form.value['deliveryDate'],
      description: form.value['description'],
      sku: form.value['sku'],
      streetAddress: form.value['streetAddress'],
      town: form.value['town']
    };
  }
}
