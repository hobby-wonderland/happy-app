import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule} from '@angular/forms';

import {ParcelAddComponent} from './parcel-add.component';
import {ParcelService} from '../domain/ports/parcel-service';
import {EMPTY} from 'rxjs';

describe('ParcelAddComponent', () => {
  let component: ParcelAddComponent;
  let fixture: ComponentFixture<ParcelAddComponent>;
  let spyParcelService: jasmine.SpyObj<ParcelService>;

  beforeEach(async () => {
    spyParcelService = jasmine.createSpyObj('ParcelService', ['fetchParcels', 'fetchParcelCountries']);
    spyParcelService.fetchParcels.and.returnValue(EMPTY);
    spyParcelService.fetchParcelCountries.and.returnValue(EMPTY);
    await TestBed.configureTestingModule({
      declarations: [ParcelAddComponent],
      imports: [ReactiveFormsModule],
      providers: [{provide: 'ParcelService', useValue: spyParcelService}]
    }).compileComponents();

    fixture = TestBed.createComponent(ParcelAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
