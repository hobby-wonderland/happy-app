import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {Parcel} from '../domain/models/parcel';
import {HappyParcelService} from '../adapters/happy-parcel.service';
import {ParcelService} from '../domain/ports/parcel-service';

@Component({
  selector: 'app-parcel-list',
  templateUrl: './parcel-list.component.html',
  styleUrls: ['./parcel-list.component.sass'],
  providers: [HappyParcelService]
})
export class ParcelListComponent implements OnInit {
  parcels: Parcel[] = [];
  parcelCountries: string[] = [];
  @ViewChild('countries') countries!: ElementRef;
  selectedCountry?: string
  enteredDescription?: string

  constructor(@Inject('ParcelService') private parcelService: ParcelService) {
  }

  ngOnInit(): void {
    this.fetchParcels();
    this.fetchParcelCountries();
  }

  fetchParcels(): void {
    if (this.selectedCountry && this.enteredDescription) {
      this.parcelService
        .fetchParcels(this.selectedCountry, this.enteredDescription)
        .subscribe((parcels) => (this.parcels = parcels));
    } else if (this.selectedCountry) {
      this.parcelService
        .fetchParcelsByCountry(this.selectedCountry)
        .subscribe((parcels) => (this.parcels = parcels));
    } else if (this.enteredDescription) {
      this.parcelService
        .fetchParcelsByDescription(this.enteredDescription)
        .subscribe((parcels) => (this.parcels = parcels));
    } else {
      this.parcelService
        .fetchParcels()
        .subscribe((parcels) => (this.parcels = parcels));
    }

  }

  fetchParcelCountries() {
    this.parcelService
      .fetchParcelCountries()
      .subscribe((countries) => (this.parcelCountries = countries));
  }

  onDescriptionEnter(description: string) {
    this.enteredDescription = description;
    this.fetchParcels();
  }

  onCountrySelect() {
    this.selectedCountry = this.countries.nativeElement.value
    this.fetchParcels();
  }
}
