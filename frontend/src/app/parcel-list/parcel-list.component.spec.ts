import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ParcelListComponent} from './parcel-list.component';
import {ParcelService} from '../domain/ports/parcel-service';
import {EMPTY} from 'rxjs';

describe('ParcelListComponent', () => {
  let component: ParcelListComponent;
  let fixture: ComponentFixture<ParcelListComponent>;
  let spyParcelService: jasmine.SpyObj<ParcelService>;

  beforeEach(async () => {
    spyParcelService = jasmine.createSpyObj('ParcelService', ['fetchParcels', 'fetchParcelCountries']);
    spyParcelService.fetchParcels.and.returnValue(EMPTY);
    spyParcelService.fetchParcelCountries.and.returnValue(EMPTY);

    await TestBed.configureTestingModule({
      declarations: [ParcelListComponent],
      providers: [{provide: 'ParcelService', useValue: spyParcelService}]
    }).compileComponents();

    fixture = TestBed.createComponent(ParcelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
