# Happy app

Parcel registration application

Consists of:

- [frontend](#frontend)
- [backend](#backend)
- postgres

## frontend

Angular SPA application built using `ng` cli.

For more info look at [Readme.md](frontend/README.md)

## backend

NestJS application built using `nest` cli.

For more info look at [Readme.md](backend/README.md)

## Local development

Install node deps:
```shell
cd frontend
npm install
```

```shell
cd backend
npm install

```

### Starting the backend

```shell
cd backend
```

```shell
npm run start:dev
```

#### For tests

```shell
npm run test
```

```shell
npm run test:watch
```

### Starting the frontend application

```shell
cd frontend
```

```shell
npm run start
```

```shell
npm run watch
```

##### For tests

```shell
npm run test
```

## Deployment

Deployment is done via docker

`Dockerfiles` are in `docker` folder

```shell
docker compose up -d --build --scale backend=3
```

With this command start up frontend, backend and supporting services
Note: `--scale backend=3` this allows to start up multiple api instances, this can be ommited.
Routing is done via `nginx`

Will start app on:

- Frontend: http://localhost:8888/
- Backend: http://localhost:8888/api
