## 1. stage: setup app dependecies
FROM node:18-alpine As development

WORKDIR /usr/src/app

COPY --chown=node:node ./backend/package*.json ./

RUN npm ci

COPY --chown=node:node ./backend/ .

USER node

## 2. stage: build production distribution
FROM node:18-alpine As build

WORKDIR /usr/src/app

COPY --chown=node:node --from=development /usr/src/app/package*.json ./

COPY --chown=node:node --from=development /usr/src/app/node_modules ./node_modules

COPY --chown=node:node ./backend/ .

# Run the build command which creates the production bundle
RUN npm run build

# Set NODE_ENV environment variable
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

# Running `npm ci` removes the existing node_modules directory and passing in --only=production ensures that only the production dependencies are installed. This ensures that the node_modules directory is as optimized as possible
RUN npm ci --omit=dev && npm cache clean --force

USER node

## 3. stage: define production deployment
FROM node:18-alpine As production

COPY --chown=node:node --from=build /usr/src/app/node_modules ./node_modules
COPY --chown=node:node --from=build /usr/src/app/dist ./dist

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

CMD [ "node", "dist/main.js" ]

