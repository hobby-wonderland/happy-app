import {Controller, Get} from '@nestjs/common';
import {AppInfoService} from './app-info.service';
import {AppInfo} from './app-info.interface';
import {Observable} from 'rxjs';

@Controller()
export class AppInfoController {
    constructor(private readonly appInfoService: AppInfoService) {
    }

    @Get()
    getInfo(): Observable<AppInfo> {
        return this.appInfoService.getInfo();
    }
}
