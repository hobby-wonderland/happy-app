import {AppInfo} from '../app-info.interface';
import {Observable} from 'rxjs';

export interface AppInfoProvider {
    getInfo(): Observable<AppInfo>;
}
