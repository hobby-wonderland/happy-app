export interface AppInfo {
    title: string;
    version: string;
}
