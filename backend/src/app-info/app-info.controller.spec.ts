import {Test, TestingModule} from '@nestjs/testing';
import {AppInfoController} from './app-info.controller';
import {AppInfoService} from './app-info.service';

describe('AppInfoController', () => {
    let controller: AppInfoController;
    let appInfoService: AppInfoService

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AppInfoController],
            providers: [AppInfoService,
                {
                    provide: AppInfoService,
                    useValue: {
                        getInfo: jest.fn()
                    }
                }
            ]
        }).compile();

        controller = module.get<AppInfoController>(AppInfoController);
        appInfoService = module.get<AppInfoService>(AppInfoService);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('should find all users ', () => {
        controller.getInfo()
        expect(appInfoService.getInfo).toHaveBeenCalled();
    });
});
