import {Test, TestingModule} from '@nestjs/testing';
import {AppInfoService} from './app-info.service';
import DoneCallback = jest.DoneCallback;

describe('AppInfoService', () => {
    let service: AppInfoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AppInfoService],
        }).compile();

        service = module.get<AppInfoService>(AppInfoService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    it('should return AppInfo', (done: DoneCallback) => {
        const expectedAppInfo = {
            title: 'Happy Parcels API',
            version: 'v0.0.0',
        };
        service.getInfo().subscribe({
            next: (appInfo) => {
                expect(appInfo).toEqual(expectedAppInfo);
                done();
            },
            error: done.fail,
        })
    });
});
