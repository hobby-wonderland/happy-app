import {Injectable} from '@nestjs/common';
import {AppInfoProvider} from './interface/app-info-provider.interface';
import {AppInfo} from './app-info.interface';
import {Observable, of} from 'rxjs';

@Injectable()
export class AppInfoService implements AppInfoProvider {
    getInfo(): Observable<AppInfo> {
        return of({
            title: 'Happy Parcels API',
            version: 'v0.0.0',
        });
    }
}
