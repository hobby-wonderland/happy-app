import {Parcel} from '../models/parcel/parcel.interface';
import {Observable} from 'rxjs';

export interface ParcelWritePort {

    /**
     * Add a new Parcel to the service
     * @param parcel new parcel
     * @returns parcel
     */
    save(parcel: Parcel): Observable<Parcel>
}