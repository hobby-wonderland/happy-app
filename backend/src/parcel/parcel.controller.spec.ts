import {Test, TestingModule} from '@nestjs/testing';
import {ParcelController} from './parcel.controller';
import {ParcelQueryPort} from './domain/ports/parcel-query.port';
import {ParcelWritePort} from './domain/ports/parcel-write.port';


describe('ParcelController', () => {
    let controller: ParcelController;
    let writeService: ParcelWritePort
    let queryService: ParcelQueryPort

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ParcelController],
            providers: [
                {
                    provide: 'ParcelWritePort',
                    useValue: {
                        save: jest
                            .fn()
                            .mockReturnValue({
                                country: 'Test country',
                                deliveryAt: new Date(2022, 12, 1),
                                description: 'Test',
                                sku: '1',
                                streetAddress: 'Test street',
                                town: 'Test town',
                                id: 'unique',
                                registeredAt: new Date(2022, 11, 1),
                                createdAt: new Date(2022, 11, 1),
                                updatedAt: new Date(2022, 11, 1),
                            })
                    }
                },
                {
                    provide: 'ParcelQueryPort',
                    useValue: {
                        fetchParcelCountries: jest.fn().mockImplementation(
                            () => ['Test', 'Rest']
                        ),
                        fetchParcels: jest
                            .fn()
                            .mockImplementation(
                                (byCountry?: string, byDescription?: string) => [{
                                    sku: '1',
                                    country: byCountry,
                                    description: byDescription
                                }, {
                                    sku: '2', country: byCountry,
                                    description: byDescription
                                }]
                            ),

                    }
                }]
        }).compile();

        controller = module.get<ParcelController>(ParcelController);
        writeService = module.get<ParcelWritePort>('ParcelWritePort');
        queryService = module.get<ParcelQueryPort>('ParcelQueryPort');
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
    it('should fetch Parcel Countries', () => {
        expect(controller.getParcelCountries()).toEqual(['Test', 'Rest'])
        expect(queryService.fetchParcelCountries).toBeCalledTimes(1)
    });
    it('should get parcels with country', () => {
        expect(controller.getParcels('Test')).toEqual([{sku: '1', country: 'Test'}, {sku: '2', country: 'Test'}])
        expect(queryService.fetchParcels).toHaveBeenNthCalledWith(1, 'Test', undefined)
    });

    it('should get parcels with description', () => {
        expect(controller.getParcels(undefined, 'Test'))
            .toEqual(
                [
                    {
                        sku: '1',
                        description: 'Test'
                    },
                    {
                        sku: '2',
                        description: 'Test'
                    }]
            )
        expect(queryService.fetchParcels).toHaveBeenNthCalledWith(1, undefined, 'Test')
    });

    it('should get parcels with country and description', () => {
        expect(controller.getParcels('Test', 'Test'))
            .toEqual(
                [
                    {
                        sku: '1',
                        description: 'Test',
                        country: 'Test'
                    },
                    {
                        sku: '2',
                        description: 'Test',
                        country: 'Test'
                    }]
            )
        expect(queryService.fetchParcels).toHaveBeenNthCalledWith(1, 'Test', 'Test')
    });

    it('should save Parcel', () => {
        expect(controller.add({
            country: 'Test country',
            deliveryAt: new Date(2022, 12, 1),
            description: 'Test',
            sku: '1',
            streetAddress: 'Test street',
            town: 'Test town'
        })).toEqual({
            country: 'Test country',
            deliveryAt: new Date(2022, 12, 1),
            description: 'Test',
            sku: '1',
            streetAddress: 'Test street',
            town: 'Test town',
            id: 'unique',
            registeredAt: new Date(2022, 11, 1),
            createdAt: new Date(2022, 11, 1),
            updatedAt: new Date(2022, 11, 1),
        })
        expect(writeService.save).toHaveBeenNthCalledWith(1, {
            country: 'Test country',
            deliveryAt: new Date(2022, 12, 1),
            description: 'Test',
            sku: '1',
            streetAddress: 'Test street',
            town: 'Test town'
        })
    });
});
