import {Parcel} from '../domain/models/parcel/parcel.interface';

/**
 * Mock parcels data for POC
 */
export const MOCK_PARCELS: Parcel[] = [
    {
        sku: '1',
        description: 'First parcel with toys',
        streetAddress: 'Tehnika 1',
        town: 'Tallinn',
        country: 'Estonia',
        deliveryAt: new Date(2022, 3, 11),
        registeredAt: new Date(2022, 3, 7),
    },
    {
        sku: '2',
        description: 'Second parcel',
        streetAddress: 'Jaama 1',
        town: 'Tartu',
        country: 'Estonia',
        deliveryAt: new Date(2022, 5, 16),
        registeredAt: new Date(2022, 4, 11),
    },
    {
        sku: '3',
        description: 'Third parcel',
        streetAddress: 'Zeltiņu 10',
        town: 'Riga',
        country: 'Latvia',
        deliveryAt: new Date(2022, 6, 11),
        registeredAt: new Date(2022, 6, 10),
    },
    {
        sku: '4',
        description: 'Fourth parcel',
        streetAddress: 'Brucenes 25',
        town: 'Riga',
        country: 'Latvia',
        deliveryAt: new Date(2022, 6, 20),
        registeredAt: new Date(2022, 6, 10),
    },
    {
        sku: '5',
        description: 'Fifth parcel with toys',
        streetAddress: 'Sinipiianpolku 3',
        town: 'Helsinki',
        country: 'Finland',
        deliveryAt: new Date(2022, 3, 4),
        registeredAt: new Date(2022, 2, 25),
    },
    {
        sku: '6',
        description: 'Sixth parcel',
        streetAddress: 'Kunnantie 2A',
        town: 'Helsinki',
        country: 'Finland',
        deliveryAt: new Date(2022, 3, 12),
        registeredAt: new Date(2022, 3, 5),
    },
    {
        sku: '7',
        description: 'Seventh parcel',
        streetAddress: 'Katarina Bangata 65',
        town: 'Stockholm',
        country: 'Sweden',
        deliveryAt: new Date(2022, 3, 11),
        registeredAt: new Date(2022, 3, 4),
    },
    {
        sku: '8',
        description: 'Eight parcel',
        streetAddress: 'Frejagatan 7A',
        town: 'Kalmar',
        country: 'Sweden',
        deliveryAt: new Date(2022, 3, 4),
        registeredAt: new Date(2022, 3, 1),
    },
    {
        sku: '9',
        description: 'Ninth parcel with toys',
        streetAddress: 'Tehnika 1',
        town: 'Narva',
        country: 'Estonia',
        deliveryAt: new Date(2022, 3, 5),
        registeredAt: new Date(2022, 2, 30),

    },
];
