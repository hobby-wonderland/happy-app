import {Body, Controller, Get, Inject, Post, Query} from '@nestjs/common';

import {Observable} from 'rxjs';
import {Parcel} from './domain/models/parcel/parcel.interface';
import {ParcelQueryPort} from './domain/ports/parcel-query.port';
import {ParcelWritePort} from './domain/ports/parcel-write.port';

@Controller()
export class ParcelController {
    constructor(
        @Inject('ParcelQueryPort') private readonly parcelQueryPort: ParcelQueryPort,
        @Inject('ParcelWritePort') private readonly parcelWritePort: ParcelWritePort
    ) {
    }

    @Get()
    getParcels(
        @Query('byCountry') country?: string,
        @Query('byDescription') description?: string
    ): Observable<Parcel[]> {
        return this.parcelQueryPort.fetchParcels(country, description);
    }

    @Get('countries')
    getParcelCountries(): Observable<string[]> {

        return this.parcelQueryPort.fetchParcelCountries();
    }

    @Post()
    add(@Body() parcel: Parcel): Observable<Parcel> {
        return this.parcelWritePort.save(parcel);
    }
}
