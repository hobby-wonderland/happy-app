import {Injectable} from '@nestjs/common';
import {from, map, Observable} from 'rxjs';
import {Parcel} from '../../domain/models/parcel/parcel.interface';
import {ParcelQueryPort} from '../../domain/ports/parcel-query.port';
import {InjectRepository} from '@nestjs/typeorm';
import {ParcelSchema} from '../database/schema/parcle.schema';
import {Repository, SelectQueryBuilder} from 'typeorm';

@Injectable()
export class ParcelQueryService implements ParcelQueryPort {

    constructor(
        @InjectRepository(ParcelSchema)
        private parcelRepository: Repository<Parcel>,
    ) {
    }

    fetchParcels(
        byCountry?: string,
        byDescription?: string
    ): Observable<Parcel[]> {

        const query: SelectQueryBuilder<Parcel> = this.parcelRepository
            .createQueryBuilder('parcel')
            .orderBy('parcel.country = :country', 'DESC')
            .setParameter('country', 'Estonia')
            .addOrderBy('delivery_at')

        if (byCountry && byDescription) {
            query.where('parcel.country = :byCountry', {byCountry})
                .andWhere('parcel.description ILIKE :byDescription', {byDescription: `%${byDescription}%`})
        } else if (byCountry) {
            query.where('parcel.country = :byCountry', {byCountry})
        } else if (byDescription) {
            query.where('parcel.description ILIKE :byDescription', {byDescription: `%${byDescription}%`})
        }
        const promise = query.getMany()
        return from(promise);
    }

    fetchParcelsByCountry(byCountry: string): Observable<Parcel[]> {
        return this.fetchParcels(byCountry, undefined);
    }

    fetchParcelsByDescription(byDescription: string): Observable<Parcel[]> {
        return this.fetchParcels(undefined, byDescription);
    }

    fetchParcelCountries(): Observable<string[]> {
        const promise = this.parcelRepository
            .createQueryBuilder()
            .select('country')
            .distinct(true)
            .orderBy('country')
            .getRawMany<ParcelCountriesResult>()

        return from(promise).pipe(map(value => value.map(value1 => value1.country)));
    }
}

interface ParcelCountriesResult {
    country: string

}
