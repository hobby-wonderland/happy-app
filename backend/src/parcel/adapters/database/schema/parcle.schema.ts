import {EntitySchema, EntitySchemaColumnOptions} from 'typeorm'
import {Parcel} from '../../../domain/models/parcel/parcel.interface';
import {BaseSchema} from './base.schema';

export const ParcelSchema = new EntitySchema<Parcel>({
    name: 'Parcel',
    tableName: 'parcel',
    columns: {
        ...BaseSchema,
        sku: {
            type: 'text'
        } as EntitySchemaColumnOptions,
        description: {
            type: 'text',
        } as EntitySchemaColumnOptions,
        country: {
            type: 'text',
        } as EntitySchemaColumnOptions,
        town: {
            type: 'text',
        } as EntitySchemaColumnOptions,
        streetAddress: {
            name:'street_address',
            type: 'text',
        } as EntitySchemaColumnOptions,
        deliveryAt: {
            name:'delivery_at',
            type: 'timestamp with time zone',
        } as EntitySchemaColumnOptions,
        registeredAt: {
            name:'registered_at',
            type: 'timestamp with time zone',
        } as EntitySchemaColumnOptions,
    },
    uniques: [
        {
            name: 'parcel_sku_key',
            columns: ['sku'],
        },
    ],

});
