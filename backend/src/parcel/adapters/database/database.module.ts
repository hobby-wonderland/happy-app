import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {PostgresConnectionOptions} from 'typeorm/driver/postgres/PostgresConnectionOptions';
import * as path from 'path';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                type: 'postgres',
                logging: true,
                host: configService.get('DATABASE_HOST'),
                port: configService.get('DATABASE_PORT'),
                username: 'postgres',
                password: 'postgres',
                database: 'postgres',
                entities: [
                    path.resolve(`${__dirname}/../../**/**.schema{.ts,.js}`)],
                migrations: [
                    path.resolve(`${__dirname}/../../../database/migrations/*{.ts,.js}`)
                ],
                schema: 'public',
                synchronize: true,
                dropSchema: true
            } as PostgresConnectionOptions),
            inject: [ConfigService]
        })
    ],
    exports: [TypeOrmModule]
})
export class DatabaseModule {
}