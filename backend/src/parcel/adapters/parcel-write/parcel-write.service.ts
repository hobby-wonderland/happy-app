import {Injectable} from '@nestjs/common';
import {ParcelWritePort} from '../../domain/ports/parcel-write.port';
import {Parcel} from '../../domain/models/parcel/parcel.interface';
import {from, Observable} from 'rxjs';
import {MOCK_PARCELS} from '../../mock/mock-parcels';
import {InjectRepository} from '@nestjs/typeorm';
import {ParcelSchema} from '../database/schema/parcle.schema';
import {Repository} from 'typeorm';

@Injectable()
export class ParcelWriteService implements ParcelWritePort {

    constructor(@InjectRepository(ParcelSchema)
                private parcelRepository: Repository<Parcel>,) {
    }

    save(parcel: Parcel): Observable<Parcel> {
        parcel.updatedAt = new Date()
        parcel.createdAt = new Date()
        parcel.registeredAt = new Date()
        const parcelPromise = this.parcelRepository.save<Parcel>(parcel);
        return from(parcelPromise);
    }
}
