import {Test, TestingModule} from '@nestjs/testing';
import {ParcelWriteService} from './parcel-write.service';
import {Parcel} from '../../domain/models/parcel/parcel.interface';
import {ParcelSchema} from '../database/schema/parcle.schema';
import {getRepositoryToken} from '@nestjs/typeorm';
import DoneCallback = jest.DoneCallback;

describe('ParcelWriteService', () => {
    let service: ParcelWriteService;

    const mockeParcel = {
        'country': 'Test country',
        'createdAt': {},
        'deliveryAt': {},
        'description': 'Test name',
        'registeredAt': {},
        'sku': '12',
        'streetAddress': 'Test street address',
        'town': 'Test town',
        'updatedAt': {}
    };
    const mockedRepo = {
        // mock the repo `findOneOrFail`
        save: jest.fn(({}) => Promise.resolve(mockeParcel)),
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ParcelWriteService,
                {
                    provide: getRepositoryToken(ParcelSchema),
                    useValue: mockedRepo,
                },],
        }).compile();

        service = module.get<ParcelWriteService>(ParcelWriteService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    it('should add a new parcel to the service', (done: DoneCallback) => {
        const newParcel: Parcel = {
            sku: '12',
            description: 'Test name',
            country: 'Test country',
            town: 'Test town',
            streetAddress: 'Test street address',
            deliveryAt: new Date(2023, 4, 7),
            registeredAt: new Date(2023, 4, 1),
            createdAt: new Date(2023, 4, 1),
            updatedAt: new Date(2023, 4, 1),
        };
        const saveSpy = jest.spyOn(mockedRepo, 'save');
        service.save(newParcel).subscribe({
            next: () => {
                expect(saveSpy).toBeCalledTimes(1)
                done();
            },
            error: done.fail,
        });
    });
});
