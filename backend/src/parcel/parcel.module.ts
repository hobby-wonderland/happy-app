import {Module} from '@nestjs/common';
import {ParcelController} from './parcel.controller';
import {ParcelWriteService} from './adapters/parcel-write/parcel-write.service';
import {ParcelQueryService} from './adapters/parcel-query/parcel-query.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ParcelSchema} from './adapters/database/schema/parcle.schema';

@Module({
    imports: [TypeOrmModule.forFeature([ParcelSchema])],
    providers: [
        {
            provide: 'ParcelWritePort',
            useClass: ParcelWriteService
        },
        {
            provide: 'ParcelQueryPort',
            useClass: ParcelQueryService
        }
    ],
    controllers: [ParcelController],
})
export class ParcelModule {
}
