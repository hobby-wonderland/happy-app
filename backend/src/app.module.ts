import {Module} from '@nestjs/common';
import {RouterModule} from '@nestjs/core';
import {AppInfoModule} from './app-info/app-info.module';
import {ParcelModule} from './parcel/parcel.module';
import {DatabaseModule} from './parcel/adapters/database/database.module';
import {ConfigModule} from '@nestjs/config';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: `${process.cwd()}/${process.env.NODE_ENV}.env`,
            isGlobal: true
        }),
        DatabaseModule,
        AppInfoModule,
        ParcelModule,
        RouterModule.register([
            {
                path: 'api',
                module: AppInfoModule,
                children: [
                    {
                        path: 'parcel',
                        module: ParcelModule,
                    },
                ],
            },
        ]),
    ],

})
export class AppModule {
}
